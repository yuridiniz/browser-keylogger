var background = {

    exibirDados: function () {
        var db = new Database();
        db.listarInputs(background.exibirInputs, null)
    },

    exibirInputs: function (e, result) {
        var table = document.createElement("table")
        table.style.width = "100%"
        document.querySelector("body").appendChild(table)

        var html = "";
        html += "<tr>";
        html += "<th>Data</th>";
        html += "<th>Host</th>";
        html += "<th>Field name</th>";
        html += "<th>Value</th>";
        html += "<th>Url</th>";
        html += "</tr>";

        for(var i = 1; i < result.rows.length; i++)
        {
            var inputData = result.rows.item(i);
            html += "<tr>";
            html += "<td>" + new Date(inputData.data).getFullTime() + "</td>";
            html += "<td>" + inputData.dominio + "</td>";
            html += "<td>" + inputData.campo + "</td>";
            html += "<td>" + inputData.valor + "</td>";
            html += "<td>" + inputData.url + "</td>";
            html += "</tr>";
        }

        table.innerHTML = html

    },

    exibirForm: function (e, result) {
    }
}


background.exibirDados();