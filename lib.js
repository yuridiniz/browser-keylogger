﻿
/*
* Obtém a data e hora formatada
*/
Date.prototype.getFullTime = function () {
    var dia = this.getDate();
    if (dia < 10)
        dia = "0" + dia;

    var mes = (parseInt(this.getMonth()) + 1);
    if (mes < 10)
        mes = "0" + mes;

    return mes + "/" + dia + "/" + this.getFullYear() + ", " + this.getHours() + ":" + this.getMinutes();
}

/*
* Executa o click em um HTML
*/
HTMLObjectElement.prototype.ExecutarClick = function () {
    var elemento = this
    if (typeof elemento == "object" && elemento != null) {
        var ev = document.createEvent("MouseEvents");
        ev.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 2, null);
        elemento.dispatchEvent(ev);
    }
    else if (typeof elemento == "string") {
        var ev = document.createEvent("MouseEvents");
        ev.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 2, null);
        var btn = document.querySelector(elemento)

        if (btn != null)
            btn.dispatchEvent(ev);
    }

    return elemento;
};

/*
* Executar serialize para um formulário
*/
HTMLFormElement.prototype.serialize = function () {
    var form = this;
    if (!form || form.nodeName !== "FORM") {
        return;
    }
    var i, j,
		obj = {};
    for (i = form.elements.length - 1; i >= 0; i = i - 1) {
        if (form.elements[i].name === "") {
            continue;
        }
        switch (form.elements[i].nodeName) {
            case 'INPUT':
                switch (form.elements[i].type) {
                    case 'text':
                    case 'hidden':
                    case 'password':
                    case 'button':
                    case 'reset':
                    case 'submit':
                        obj[form.elements[i].name] = encodeURIComponent(form.elements[i].value);
                        break;
                    case 'checkbox':
                    case 'radio':
                        if (form.elements[i].checked) {
                            obj[form.elements[i].name] = encodeURIComponent(form.elements[i].value);
                        }
                        break;
                    case 'file':
                        break;
                }
                break;
            case 'TEXTAREA':
                obj[form.elements[i].name] = encodeURIComponent(form.elements[i].value);
                break;
            case 'SELECT':
                switch (form.elements[i].type) {
                    case 'select-one':
                        obj[form.elements[i].name] = encodeURIComponent(form.elements[i].value);
                        break;
                    case 'select-multiple':
                        for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {
                            if (form.elements[i].options[j].selected) {
                                obj[form.elements[i].name] = encodeURIComponent(form.elements[i].options[j].value);
                            }
                        }
                        break;
                }
                break;
            case 'BUTTON':
                switch (form.elements[i].type) {
                    case 'reset':
                    case 'submit':
                    case 'button':
                        obj[form.elements[i].name] = encodeURIComponent(form.elements[i].value);
                        break;
                }
                break;
        }
    }
    return obj;
};