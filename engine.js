/*
* Cria um ouvinte de mensagens, recebe as mensagens enviada pelos content_scripts e exibe o rich notification
*
*/
var socket = null;

var EnviarFormulario = function (dados) {
    var db = new Database();
    db.inserirForm(dados)
}

var EnviarInput = function (dados) {
    var db = new Database();
    db.inserirInput(dados)
}

chrome.runtime.onConnect.addListener(function (port) {
    socket = port;
    port.onMessage.addListener(function (msg) {
        console.log(msg)

        if (msg.status == 200)
            socket.postMessage("Conectado!")

        if (msg.callback == "AbrirAlerta")
            AbrirAlerta(msg);

        if (msg.callback == "GravaInput")
            EnviarInput(msg.dados);

        if (msg.callback == "GravarFormulario")
            EnviarFormulario(msg.dados);


        if (msg.callback == "VerificarAtualizacao")
            VerificarAtualizacao();
    });
});

chrome.browserAction.onClicked.addListener(function (callback) {

    chrome.tabs.create({ url: chrome.extension.getURL("relatorio/window.html") });
});

chrome.notifications.onButtonClicked.addListener(function(notfiId, btnIndex)
{
    if (btnIndex == 0)
		chrome.tabs.create({ url: chrome.app.getDetails().update_url.toString().replace("update.xml","YoutubeTools.zip") });
});

var AbrirAlerta = function(msg) {

    if (msg.from == undefined) return;
    if (msg.id == undefined) msg.id = msg.from ;
    if(msg.body == undefined) return;
    if(msg.title == undefined) return;
    if (msg.btnList == undefined) msg.btnList = [];

	msg.id = msg.id.toString()

	chrome.notifications.clear(msg.id, function(e) {
	    console.log(e)
	});

    chrome.notifications.create(msg.id, {
        type: 'basic',
        title: msg.title,
        iconUrl: chrome.extension.getURL('img/icon128.png'),
        message: msg.body,
        priority: 1,
        isClickable: true,
        buttons: msg.btnList
    }, function(id) {
        console.log("callback")
    });
}

